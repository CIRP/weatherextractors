import json
import re
import requests
from bs4 import BeautifulSoup

try:
    # We are attempting to run via cli within mFisheries application
    from mfisheries.modules.fewer.weather.parsers.WeatherSourceExtractor import WeatherSourceExtractor
except:
    # We are attempting to run via cli outside of application
    from WeatherSourceExtractor import WeatherSourceExtractor


class Extractor(WeatherSourceExtractor):
    def __init__(self):
        WeatherSourceExtractor.__init__(self,"http://www.weather.gov.dm/forecast")

    def get_poster_url(self):
        return self.post_url

    def get_extractor_url(self):
        return self.extractor_url

    def get_reading_types(self):
        return {
            "wind": {
                "type": "text",
                "unit": "km/h"
            },
            "seas": {
                "type": "text",
                "unit": "m"
            }
        }

    def extract(self):
        self.readings = {}

        # Process the information from the current url
        r = requests.get(self.extractor_url)
        items = []
        data = []

        soup = BeautifulSoup(r.content, "html.parser")
        # try:
        outlook = soup.find_all("div",{"class": "outlook_da_la col-sm-6"})
        # print(outlook[0].text.strip)
        try:
            self.readings['outlook'] = self.clean_text(outlook[0].text.strip())
        except Exception, e:
            print("Error while attempting to retrieve outlook {0}".format(e))

        for div in soup.find_all("div",{"itemprop": "articleBody"}):
            for d in div.find_all("div"):
                for p in d.find_all("p"):
                    print("Item")
                    print(p.text)
                    items.append(self.clean_text(p.text))
           # print("Body : ")
            # print(body)

        # print("ITEMS : ")
        # print(items)
        for i in items:
            try:
                self.readings['synopsis'] = self.findword("synopsis",items)[0].split(": ")[1].replace(u'\xa0', ' ')
            except Exception, e:
                print("Error while attempting to retrieve synopsis {0}".format(e))

            try:
                self.readings['wind'] = self.findword("wind",items)[0].split(": ")[1].replace(u'\xa0', ' ')
            except Exception, e:
                print("Error while attempting to retrieve wind {0}".format(e))

            try:
                self.readings['seas'] = self.findword("sea",items)[0].split(": ")[1].replace(u'\xa0', ' ')
            except Exception, e:
                print("Error while attempting to retrieve sea {0}".format(e))

            try:
                self.readings['sea conditions'] = self.findword("sea conditions",items)[0].split(": ")[1].replace(u'\xa0', ' ')
            except Exception, e:
                print("Error while attempting to retrieve sea conditions {0}".format(e))

            try:
                self.readings['advisory'] = self.findword("advisory",items)[0].split(": ")[1].replace(u'\xa0', ' ')
            except Exception, e:
                print("Error while attempting to retrieve sea conditions {0}".format(e))

            try:
                self.readings['sunrise'] = self.findword("sunrise",items)[0].split(": ")[1].replace(u'\xa0', ' ')
            except Exception, e:
                print("Error while attempting to retrieve sunrise {0}".format(e))

            try:
                self.readings['sunset'] = self.findword("sunset",items)[0].split(": ")[1].replace(u'\xa0', ' ')
            except Exception, e:
                print("Error while attempting to retrieve sunset {0}".format(e))

            try:
                self.readings['low tide'] = self.findword("low tide",items)[0].split(": ")[1].replace(u'\xa0', ' ')
            except Exception, e:
                print("Error while attempting to retrieve low tide {0}".format(e))

            try:
                self.readings['high tide'] = self.findword("high tide",items)[0].split(": ")[1].replace(u'\xa0', ' ')
            except Exception, e:
                print("Error while attempting to retrieve low tide {0}".format(e))

            try:
                self.readings['forecast'] = self.findword("forecast",items)[0].strip().replace(u'\xa0', ' ')
            except Exception, e:
                print("Error while attempting to retrieve forecast {0}".format(e))



        # self.readings['tonight'] = self.findword("tonight",items)[0].split("tonight: ")[1]
        #
        #
        #

        print("readings")

        print (self.readings)



        return self.readings

        # except Exception, e:
        #     print("Error retrieving article ".format(e))

    def toJSON(self):
        return json.dumps(self.readings)


if __name__ == "__main__":
    country = Extractor()
    country.extract()
    # print(country.get_reading_types())
    # print(country.toJSON())
