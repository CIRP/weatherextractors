import json
import re
import requests
from bs4 import BeautifulSoup


try:
	# We are attempting to run via cli within mFisheries application
	from mfisheries.modules.fewer.weather.parsers.WeatherSourceExtractor import WeatherSourceExtractor
except:
	# We are attempting to run via cli outside of application
	from WeatherSourceExtractor import WeatherSourceExtractor



class Extractor(WeatherSourceExtractor):
	def __init__(self):
		WeatherSourceExtractor.__init__(
			self,
			"http://meteo.gov.vc/meteo/",

		)

	def get_poster_url(self):
		return self.post_url

	def get_extractor_url(self):
		return self.extractor_url

	def get_reading_types(self):
		return {
			# "temperature": {
			# "type": "numerical",
			# "unit": "F"
			# },
			"wind": {
				"type": "numerical",
				"unit": "km/h"
			},
			"seas": {
				"type": "numerical",
				"unit": "feet"
			}
			# "pressure": {
			# 	"type": "numerical",
			# 	"unit": "hPa"
			# },
			# "Relative Humidity": {
			# 	"type": "numerical",
			# 	"unit": "%"
			# }
		}

	def extract(self):
		headings = []
		details = []
		self.readings = {}
		count = 1
		index = 0
		r = requests.get(self.extractor_url)
		soup = BeautifulSoup(r.content.decode('utf-8', 'ignore'), "html.parser")

		# Strategy for caching request while developing
		# filename = "input.html"
		# with open(filename, "r") as fp:
			# fp.write(str(soup))
			# content = fp.read()
			# fp.close()
		# soup = BeautifulSoup(content, "html.parser")

		# Current weather on right side
		data = soup.find_all("ul")
		save = ""
		for row in data:
			# takes the block of text with readings alone. the ul has other unrelated stuff we didnt need
			if "Temperature" in row.text:
				save = row.text
				break
		for current in save.strip().split("\n"):
			# self.readings[current.replace(u'\xa0', u' ').split(": ")[0]] = current.replace(u'\xa0', u' ').split(": ")[1]
			try:
				print(current.replace(u'\xa0', u' '))
				self.readings[current.replace(u'\xa0', u' ').split(": ")[0]] = current.replace(u'\xa0', u' ').split(": ")[1]
			except Exception, e:
				print("Error while attempting to retrieve current value {0}".format(e))
		# Table latest forecast data
		data = soup.find_all("td")
		for row in data:
			if count % 2 == 1:
				headings.append(row.text.strip())
				count = count + 1
			else:
				details.append(row.text.strip())
				count = count + 1

		# Tides formatted different
		for key in headings:
			if 'tides' in key.lower():
				break
			self.readings[key.replace(":", "")] = details[index]
			index += 1

		# getting tides seperate
		tides = []
		tide = soup.find_all("tr")
		for item in tide:
			if 'Tides' in item.text:
				tides = item.text.rstrip().split("\n")

		tideData = ""
		# Splitting by the High and Low to seperate them then concatenate them as a string removing the excess * on site
		try:
			index = tides.index("High:")
			if tides[index + 1] != "****" and tides[index + 1] != '':
				tideData += "High: " + self.clean_text(tides[index + 1])

			if tides[index + 2] != "****" and tides[index + 2] != '':
				tideData += " High: " + self.clean_text(tides[index + 2])

			index = tides.index("Low:")
			if tides[index + 1] != "****" and tides[index + 1] != '':
				tideData += " Low: " + self.clean_text(tides[index + 1])

			if tides[index + 2] != "****" and tides[index + 2] != '':
				tideData += " Low: " + self.clean_text(tides[index + 2])

			# set the string to Tides
			self.readings["Tides"] = tideData
		except Exception, e:
			print("Error occurred in St Vincent Current and Forecast Extractor: {0}".format(e))

		return self.readings

	def toJSON(self):
		return json.dumps(self.readings)


# Can be executed directly using the command python -m mfisheries.modules.weather.parsers.StVincent
if __name__ == "__main__":
	country = Extractor()
	country.extract()
	print country.toJSON()
