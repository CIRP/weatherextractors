import os
from utils import display_debug

from os.path import dirname, basename, isfile, realpath
import glob
modules = glob.glob(dirname(realpath(__file__))+"/*.py")
__all__ = [ basename(f)[:-3] for f in modules if isfile(f) and not f.endswith('__init__.py')]



def get_all_package_names(path="extractors"):
	#  this is a list of Extractors available
	packages = []
	base_path = os.path.dirname(os.path.realpath(__file__))
	extractor_path = "{0}/{1}/".format(base_path, path)

	for i in os.listdir(extractor_path):
		if len(i.split(".")) == 2:
			if i.split(".")[1] == "py":
				packages.append(i.split(".")[0])
	return packages


def extractor_factory(extractor_type, path="extractors"):
	packages = get_all_package_names(path)
	country = str(extractor_type).split("_")[0]
	try:
		display_debug("Attempting to process: " + str(extractor_type), type="info", module_name=__name__)
		if extractor_type in packages:
			display_debug("Attempting to run: " + str(extractor_type + "." + "Extractor()"), type="info", module_name=__name__)
			call = eval(extractor_type + "." + "Extractor()")
			call.extract()
			return call
	except Exception, e:
		display_debug(e, type="error", module_name=__name__)
		send_email(str(e), country, str(extractor_type))
	return None


if __name__ == "__main__":
	packages = get_all_package_names("./")
	for package in packages:
		if "WorldTides" not in package and "MainTester" not in package and "WeatherSourceExtractor" not in package:
			print("Loading Data for: " + package)
			module = __import__(package)
			weather_class = getattr(module, "Extractor")
			weather_instance = weather_class()
			weather_instance.extract()
			output = weather_instance.toJSON()
			with open("./output/{0}.json".format(package), "w") as fp:
				fp.write(output)
				fp.close()