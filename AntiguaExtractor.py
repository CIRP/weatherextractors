import json
import re
import requests
from bs4 import BeautifulSoup

try:
	# We are attempting to run via cli within mFisheries application
	from mfisheries.modules.fewer.weather.parsers.WeatherSourceExtractor import WeatherSourceExtractor
except:
	# We are attempting to run via cli outside of application
	from WeatherSourceExtractor import WeatherSourceExtractor


class Extractor(WeatherSourceExtractor):
	def __init__(self):
		WeatherSourceExtractor.__init__(self, "http://www.antiguamet.com/Antigua_Met_files/Daily_FCast.html")

	def get_poster_url(self):
		return self.post_url

	def get_extractor_url(self):
		return self.extractor_url

	def get_reading_types(self):
		return {
			"winds": {
				"type": "numerical",
				"unit": "mph"
			},
			"seas": {
				"type": "numerical",
				"unit": "feet"
			}
		}

	def extract(self):
		self.readings = {}
		r = requests.get(self.extractor_url)
		soup = BeautifulSoup(r.text, "html.parser")
		data = soup.find_all("p")
		tdata = soup.find_all("tr")
		for t in tdata:
			if "high" in t.text.lower():
				high = t.text.replace(" ", "")
			if "low" in t.text.lower():
				low = t.text.replace(" ", "")

		high = re.sub("[^0-9]", "", high.split("C")[0])
		self.readings['high Temp'] = high

		low = re.sub("[^0-9]", "", low.split("C")[0])
		self.readings['low Temp'] = low

		for x in data:
			if "synopsis: " in x.text.lower():
				# self.readings['synopsis'] = self.clean_text(x.text).split(": ")[1]
				self.readings['synopsis'] = self.clean_text(x.text.split(":")[1])

			if "seas:" in x.text.lower():
				self.readings['seas'] = self.clean_text(x.text.split(":")[1])

			if "winds:" in x.text.lower():
				self.readings['winds'] = self.clean_text(x.text.split(":")[1])

			if "sunset" in x.text.lower():
				self.readings['sunset'] = self.clean_text(x.text.split(":")[1])

			if "sunrise" in x.text.lower():
				self.readings['sunrise'] = self.clean_text(x.text.split(":")[1])

			if "pressure" in x.text.lower():
				self.readings['pressure'] = self.clean_text(x.text)

			if "tomorrow" in x.text.lower():
				self.readings['tomorrow'] = self.clean_text(x.text.split(":")[1])

			if "tonight" in x.text.lower():
				self.readings['tonight'] = self.clean_text(x.text.split(":")[1])

		return self.readings

	def toJSON(self):
		return json.dumps(self.readings)

		# Can be executed directly using the command python -m mfisheries.modules.weather.parsers.GrenadaCurrentExtractor


if __name__ == "__main__":
	country = Extractor()
	print(country.get_reading_types())
	country.extract()
	print(country.toJSON())
